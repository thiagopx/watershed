import numpy as np
from collections import deque

'''
Implementation by Thiago Paixao (paixao@gmail.com) of the watershed algorithm
proposed in

Vincent, Luc, and Pierre Soille.
"Watersheds in digital spaces: an efficient algorithm based on immersion
simulations." IEEE transactions on pattern analysis and machine intelligence
13.6 (1991): 583-598.
'''

def watershed(image, mask=None):

    assert image.dtype == np.int64

    # 8-neighborhood of a pixel excluding itself
    neighbors = lambda y, x: [(y - 1, x - 1), (y - 1, x), (y - 1, x + 1),
                              (y, x - 1), (y, x + 1), (y + 1, x - 1),
                              (y + 1, x), (y + 1, x + 1)]

    # Codes
    INIT = -3
    MASK = -2 # mask pixels for flooding
    WSHED = 0
    NON_INTEREST_REGION = -1

    # Labeled catchment basins
    labels = np.full_like(image, INIT, dtype=np.float64)

    # Minimum and maximum values of image (non-negative values)
    min_level, max_level = image.min(), image.max()
    
    # Masking input image and resulting labels
    if mask is not None:
        inv_mask = ~ mask
        image[inv_mask] = NON_INTEREST_REGION
        labels[inv_mask] = NON_INTEREST_REGION

    # Pad images (avoid check image limits)
    image = np.pad(image, (1, 1), 'constant', constant_values=INIT)
    labels = np.pad(labels, (1, 1), 'constant', constant_values=INIT)

    current_label = 0
    distances = np.zeros_like(image)
    pixels_queue = deque()

    for level in xrange(min_level, max_level + 1):
        '''
        Geodesic SKIZ of level h - 1 inside level h

        Process new level h. Put into the queue all the pixels having a
        neighbourhood (distance 1) with labeled (WSHED or INTEGER) pixels.
        '''

        pixels_by_level = zip(* np.where(image == level))
        for p in pixels_by_level:
            labels[p] = MASK
            N = neighbors(* p)
            for q in N:
                if labels[q] >= WSHED:
                    distances[p] = 1
                    pixels_queue.append(p)
                    
        # Distance of some pixel in queue to a defined pixel
        current_distance = 1
        pixels_queue.append(None)

        '''
        Repeat indefinitely in order to define each pixel in queue according
        to the already defined pixels. This process try to label only pixels
        with graylevel less than or equal to level
        '''

        while True:
            p = pixels_queue.popleft()

            '''
            No more pixels whose distance to a defined pixel is less than
            current distance
            '''
            if p is None:
                # No more pixels to process at this level?
                if not pixels_queue:
                    break
                else:
                    # Start analysis to the next distance
                    pixels_queue.append(None)   
                    current_distance += 1
                    p = pixels_queue.popleft()

            # 8-Neighborhood of respective pixel (coordinates)
            for q in neighbors(* p):
                if distances[q] < current_distance and labels[q] >= WSHED:
                # q belongs to an existing basin or to watersheds
                    if labels[q] > WSHED:
                        if labels[p] == MASK or labels[p] == WSHED:
                            labels[p] = labels[q]
                        elif labels[p] != labels[q]:
                            labels[p] = WSHED
                    elif labels[p] == MASK:
                        labels[p] = WSHED
                        
                elif labels[q] == MASK and distances[q] == 0:  # plateau
                    distances[q] = current_distance + 1
                    pixels_queue.append(q)

        # Detect and process new minima at level h
        for p in pixels_by_level:
            distances[p] = 0  # reset distance

            # If the current pixel is undefined, it is inside a new minimum
            if labels[p] == MASK:
                current_label += 1
                pixels_queue.append(p)
                labels[p] = current_label
                    
                while pixels_queue:
                    q = pixels_queue.popleft()
                 
                    # 8-Neighborhood of respective pixel (coordinates)
                    for r in neighbors(* q):
                        if labels[r] == MASK:
                            pixels_queue.append(r)
                            labels[r] = current_label
        
    # Setting watershed lines	
    labeled_pixels = zip(* np.where(labels > WSHED))
    for p in labeled_pixels:
        for q in neighbors(* p):
            if labels[q] > WSHED and labels[q] < labels[p]:
                labels[p] = WSHED
                break

    # Removing extra space
    labels = labels[1:-1, 1:-1]
    
    # Watershed barriers
    lines = labels == WSHED

    return labels, lines

'''
Testing watershed

python watershed.py <option>, where <option> can be 1 or 2.
'''
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import ImageGrid
    from skimage import color
    import sys
    
    def test1():
        '''
        Test 1: two black balls centered at (x1, y1) and (x2, y2) with radius R1
        and R2, respectively.
        '''
        Y, X = np.indices((400, 400))
        x1, y1 = (100, 100)
        x2, y2= (200, 200)
        R1, R2 = 20, 40
        ball1 = ((X - x1)**2 + (Y - y1)**2) > R1 ** 2
        ball2 = ((X - x2)**2 + (Y - y2)**2) >= R2 ** 2
        image = ball1 & ball2
        labels, lines = watershed(image.astype(np.int64))
        
        wshed_image = color.gray2rgb((255 * image).astype(np.uint8))
        wshed_image[lines, :] = [255, 0, 0]
    
        
        fig = plt.figure(1, (10, 10))
        grid = ImageGrid(
               fig, 111,           # similar to subplot(111)
               nrows_ncols=(1, 3), # creates 2x2 grid of axes
               axes_pad=0.1,       # pad between axes in inch.
        )
     
        grid[0].imshow(image, cmap=plt.get_cmap('gray'))
        grid[1].imshow(wshed_image)
        grid[2].imshow(labels)
        plt.show()
    
    def test2():
        import cv2
    
        image_rgb = cv2.imread('data/nuclear.png', cv2.IMREAD_COLOR)
        image_gl = cv2.imread('data/nuclear.png', cv2.IMREAD_GRAYSCALE)
        image_smoothed = cv2.GaussianBlur(image_gl, (15, 15), 0)
        
        kernel = np.ones((5, 5), np.uint8)
        gradient = cv2.morphologyEx(image_smoothed, cv2.MORPH_GRADIENT, kernel)
        labels, lines = watershed(gradient.astype(np.int64))
        wshed_image = image_rgb.copy()
        wshed_image[lines, :] = [255, 0, 0]

        fig = plt.figure(1, (10, 10))
        grid = ImageGrid(
               fig, 111,           # similar to subplot(111)
               nrows_ncols=(1, 3), # creates 2x2 grid of axes
               axes_pad=0.1,       # pad between axes in inch.
        )
     
        grid[0].imshow(image_rgb, cmap=plt.get_cmap('gray'))
        grid[1].imshow(wshed_image)
        grid[2].imshow(labels)
        plt.show()
    
    option = int(sys.argv[1])
    assert option in [1, 2]
    if option == 1:
        test1()
    else:
        test2()