# README #

This is a Python implementation of the watershed algorithm proposed in

Vincent, Luc, and Pierre Soille. "Watersheds in digital spaces: an efficient algorithm based on immersion
simulations." IEEE transactions on pattern analysis and machine intelligence
13.6 (1991): 583-598.

Author: Thiago Meireles Paixão (paixao@gmail.com)

### watershed function dependencies  ###

* Numpy

### Test dependencies ###

* Opencv 3.1
* Matplotlib

### Easy setup ###

1. Install Anconda Python distribution from https://www.continuum.io/downloads
2. Run: conda install opencv

### Testing ###
* Run: python watershed.py [option], where option can be 1 or 2.